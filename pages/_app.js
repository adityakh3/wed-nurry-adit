import "../styles/globals.css";
import { ChakraProvider } from "@chakra-ui/react";

const CustomApp = ({ Component, pageProps }) => {
  return (
    <ChakraProvider resetCSS={false}>
      <Component {...pageProps} />
    </ChakraProvider>
  );
};

export default CustomApp;
