import { Image, Container, Box } from "@chakra-ui/react";

export default function Home() {
  return (
    <Box backgroundColor="black">
      <Container height="100vh" alignItems="center" p="0">
        <Image
          height="inherit"
          objectFit="cover"
          src="images/coming_soon.png"
          alt="Dan Abramov"
        />
      </Container>
    </Box>
  );
}
